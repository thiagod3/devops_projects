resource "aws_security_group" "allow_http_https" {
    #region = "us-east-1"
    name = "teste_123"
    vpc_id = var.aws_vpc
    description = "Terraform Thiago"
    #id = var.security_id


    ingress {
        
      from_port = var.from_http
      to_port = var.to_http
      protocol = var.protocol_id
      cidr_blocks = ["0.0.0.0/0"]  

     }
     
    
    ingress {

      from_port = var.from_https
      to_port = var.to_https
      protocol = var.protocol_id
      cidr_blocks = ["0.0.0.0/0"]     
    
    }

    ingress {

      from_port = var.ssh
      to_port = var.ssh
      protocol = var.protocol_id
      cidr_blocks = ["0.0.0.0/0"]     
    
    }


    ingress {
        from_port = var.from_tom
        to_port = var.to_tom
        protocol = var.protocol_id
        cidr_blocks = ["0.0.0.0/0"]  
    }
     
      

    egress {
        from_port = var.port_egress
        to_port = var.to_port_egress
        protocol = var.protocol_egress
        cidr_blocks = ["0.0.0.0/0"]  
    }

    
     tags = {
        #team = "Pulmão"
        Name = "Teste_Security"
        #owner = "Thiago_Brito"
    }
}