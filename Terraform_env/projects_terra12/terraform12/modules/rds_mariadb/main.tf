provider "aws" {
  region = "us-east-1"
  
}
provider "mysql" {
  endpoint = aws_db_instance.app_test.endpoint
  username = var.user
  password = var.pass
}

resource "aws_db_instance" "app_test" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mariadb"
  engine_version       = "10.4.8"
  instance_class       = var.db_instance
  name                 = "app"
  username             = var.user_admin
  password             = var.pass_admin
  db_subnet_group_name = var.group_db
  vpc_security_group_ids = ["sg-0a22bc564c00ca591"]
  publicly_accessible = true
}

resource "mysql_database" "test" {
  name = "aplicacao"
}

resource "mysql_database" "test1" {
  name = "logs"
}

resource "mysql_user" "app" {
  user               = "app"
  host               = var.host
  plaintext_password = var.pass_app
}

resource "mysql_user" "api" {
  user               = "api"
  host               = var.host
  plaintext_password = var.pass_api
}

resource "mysql_user" "convidado" {
  user               = "convidado"
  host               = var.host
  plaintext_password = var.pass_convidado
}

resource "mysql_grant" "app" {
  user       = mysql_user.app.user
  host       = mysql_user.app.host
  database   = "app"
  privileges = ["SELECT"]
}

resource "mysql_grant" "api" {
  user       = mysql_user.api.user
  host       = mysql_user.api.host
  database   = "app"
  privileges = ["SELECT", "CREATE"]
}


resource "mysql_grant" "convidado" {
  user       = mysql_user.convidado.user
  host       = mysql_user.convidado.host
  database   = "aplicacao"
  privileges = ["SELECT"]
}