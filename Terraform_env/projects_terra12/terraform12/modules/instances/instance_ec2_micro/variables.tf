variable ami_id {
  default     = ""
  description = "id of ami used"
}

variable instance {
  default     = "t3a.medium"
  description = "type of instance used"
}

variable key {
  default     = ""
  description = "key to access ssh"
}


variable vpc_id {
  default = ""
}

variable sub_id {
  default = ""
}

variable public {
  default = ""
}

###### Security Group

variable aws_vpc {
  default = ""
}

variable from_http {
  default  = "80"
  
}

variable to_http {
    default = "80"

}

variable from_https {
   default = "443"
}

variable to_https {
  default = "443"
}

variable from_tom {
  default = "8080"
}

variable to_tom {
  default = "8080"
}

variable protocol_id {
  default = "tcp"

}

variable port_egress {
  default = 0
  
}

variable to_port_egress {
  default = "0"
  
}

variable protocol_egress {
  default = "-1"
  
}

variable cidr_ingress {
  default = "0.0.0.0/0"
  
}

variable ssh {
  default = "22"
}

variable cidr_egress {
  default  = "0.0.0.0/0"
  
}



